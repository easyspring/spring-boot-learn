# SpringBoot 系列教程

本教程的是系列教程, 会不定期更新. 每个项目都会尽可能独立的讲解单个知识点, 若有项目关联的情况, 也会在教程文章中说明, 详情内容可以[查看文档](https://blog.easyspring.io/%E5%90%8E%E5%8F%B0/SpringBoot/).

博文中所有的代码都会提交到 [Gitee](https://gitee.com/easyspring/spring-boot-learn)  上, 欢迎 `star`.

环境介绍:

此处环境仅供产考, 如果在测试运行中发现有运行问题, 请先查看环境.

* Java: 1.8
* SpringBoot: 2.0.0
* Maven: 3.3.9
* IDEA: 2017.2

博客的内容都会在我的[个人博客](http://blog.easyspring.io)首发. 如果你在阅读中遇到什么问题或好的建议, 欢迎在评论区留言, 也可以发送邮件到我的个人邮箱: easyspring@163.com

